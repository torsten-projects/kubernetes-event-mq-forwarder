FROM python:3.9-alpine

WORKDIR /app
ENTRYPOINT [ "python3", "-u", "-m", "forwarder" ]
COPY requirements.txt ./
RUN python3 -m pip install --upgrade pip
RUN python3 -m pip install --upgrade -r requirements.txt
COPY . .
