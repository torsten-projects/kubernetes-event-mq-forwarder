import click
import pika
import kubernetes
import kubernetes.config
import kubernetes.client
import kubernetes.client.api_client
import kubernetes.watch
import logging
import datetime
import json


@click.command()
@click.option('--rabbitmq-host', show_envvar = True)
@click.option('--rabbitmq-port', show_envvar = True, default = 5672)
@click.option('--rabbitmq-user', show_envvar = True)
@click.option('--rabbitmq-password', show_envvar = True)
@click.option('--rabbitmq-exchange', show_envvar = True)
def forward(
	rabbitmq_host: str,
	rabbitmq_port: int,
	rabbitmq_user: str,
	rabbitmq_password: str,
	rabbitmq_exchange: str,
):
	logger = logging.getLogger()
	url = f'amqp://{rabbitmq_user}:{rabbitmq_password}@{rabbitmq_host}:{rabbitmq_port}/%2F?heartbeat=180'
	connection = pika.BlockingConnection(pika.URLParameters(url))
	channel = connection.channel()
	kubernetes.config.load_incluster_config()
	with kubernetes.client.api_client.ApiClient() as api:
		core_api = kubernetes.client.CoreV1Api(api)
		watch = kubernetes.watch.Watch()
		for event in watch.stream(core_api.list_event_for_all_namespaces):
			if 'object' not in event:
				continue
			event_object = event['object']
			msg_reason: str = f'{event_object.reason}'
			msg_details: str = f'{event_object.message}'
			current_datetime: datetime.datetime = event_object.event_time
			first_datetime: datetime.datetime = event_object.first_timestamp
			last_datetime: datetime.datetime = event_object.last_timestamp
			event_datetime: datetime.datetime = current_datetime
			if event_datetime is None:
				event_datetime = last_datetime
			if event_datetime is None:
				event_datetime = first_datetime
			if event_datetime is None:
				continue
			now = datetime.datetime.now(tz = event_datetime.tzinfo)
			time_diff = now - event_datetime
			if time_diff > datetime.timedelta(seconds = 10):
				logger.info('Ignoring old: %s', msg_reason)
				continue
			message = json.dumps(
				{
					'reason': msg_reason,
					'details': msg_details,
					'datetime': event_datetime.isoformat(),
				}
			)
			logger.info('Event: %s', message)
			channel.basic_publish(rabbitmq_exchange, 'event', message)


if __name__ == '__main__':
	forward(auto_envvar_prefix = 'FORWARDER')  # pylint: disable=unexpected-keyword-arg,no-value-for-parameter
